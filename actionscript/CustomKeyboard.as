﻿package actionscript{

	import flash.display.MovieClip;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.events.*;
	import flash.text.TextField;
	import flash.text.TextFormat;

	public class CustomKeyboard extends MovieClip {

		static var keyData:XML = new XML;
		static var myKey:KeyButton = new KeyButton;
		static var keyHolder:MovieClip = new MovieClip;
		//static var textScreen:TextField = new TextField;
		static var shift:Boolean = false;
		static var caps:Boolean = false;
		static var keyvalue:String;

		public var targetText:TextField = new TextField;
		public var numberOnly:Boolean = false;
		public var maxCharacter:Number = 2000;
		
		

		public function CustomKeyboard() {
			//buildTextScreen();
			loadKeyboardinfo();
			setupKeyHolder();
		}
		static public function capsToggle():void {
			if (caps) {
				caps = false;
			} else {
				caps = true;
			}
		}
		static public function shiftToggle():void {
			if (shift) {
				shift = false;
			} else {
				shift = true;
			}
			if(caps){
				caps = false;
			}
		}
		
		
		
		/*private function buildTextScreen():void {
			textScreen.x = 5;
			textScreen.y = 5;
			textScreen.width = 640;
			textScreen.height = 190;
			textScreen.text = "";
			textScreen.wordWrap = true;
			textScreen.selectable = false;
			textScreen.border = true;
			addChild(textScreen);
		}*/
		private function loadKeyboardinfo():void {
			var xmlLoader:URLLoader = new URLLoader;
			var xmlRequester:URLRequest = new URLRequest("xml/keys.xml");
			xmlLoader.addEventListener(Event.COMPLETE,xmlLoaded);
			xmlLoader.addEventListener(IOErrorEvent.IO_ERROR,ioErrorHandler);
			xmlLoader.load(xmlRequester);
			
		}
		private function ioErrorHandler(event:IOErrorEvent):void {
			trace("ioErrorHandler: " + event);
		}
		private function xmlLoaded(event:Event):void {
			var loader:URLLoader=URLLoader(event.target);
			keyData = new XML(loader.data);
			//trace(keyData);
			buildKeyboard();
		}
		private function setupKeyHolder():void {
			keyHolder.x = 5;
			keyHolder.y = 5;
			addChild(keyHolder);
			//keyHolder.addEventListener(MouseEvent.CLICK, clicked);
		}
		private function buildKeyboard():void {
			var numKeys:Number = getNumberOfKeys();
			var xPlacement:Number = 0;
			for (var i = 0; i < keyData.row.length(); i++) {
				for (var j = 0; j < keyData.row[i].key.length(); j++) {
					var newKey:KeyButton = new KeyButton;
					newKey.code = keyData.row[i].key[j].code;
					newKey.char = keyData.row[i].key[j].char;
					newKey.name =  keyData.row[i].key[j].code;
					newKey.shiftChar = keyData.row[i].key[j].shiftChar;
					newKey.keyWidth = checkWidth(keyData.row[i].key[j].char);
					newKey.x = xPlacement + 5;
					newKey.y = (newKey.keyHeight + 5) * i;
					newKey.build();
					xPlacement += newKey.keyWidth+ 5;
					keyHolder.addChild(newKey);
					newKey.addEventListener(MouseEvent.CLICK, clicked);
				}
				xPlacement = 0;
			}
		}
		
		private function addText(string:String, shiftString:String,fp:String,ap:String) {
			if (shift) {
				fp = fp+shiftString;
				this.targetText.replaceText(0,this.targetText.length,fp+ap);
				this.targetText.setSelection(this.targetText.caretIndex+1,this.targetText.caretIndex+1);
				shift = false;
			} else if (caps) {
				fp = fp+shiftString;
				this.targetText.replaceText(0,this.targetText.length,fp+ap);
				this.targetText.setSelection(this.targetText.caretIndex+1,this.targetText.caretIndex+1);
			} else {
				
				 if(this.targetText.length != this.maxCharacter)
				 {
				if(!this.numberOnly)
				{
				  fp = fp+string;
				  this.targetText.replaceText(0,this.targetText.length,fp+ap);
				  this.targetText.setSelection(this.targetText.caretIndex+1,this.targetText.caretIndex+1);
				  //this.targetText.appendText(string);
				}
				else
				{
					if(string == "0" || string == "1" || string == "2" || string == "3" || string == "4" || string == "5" || string == "6" || string == "7" || string == "8" || string == "9")
					{
						fp = fp+string;
				        this.targetText.replaceText(0,this.targetText.length,fp+ap);
						this.targetText.setSelection(this.targetText.caretIndex+1,this.targetText.caretIndex+1);
						}
					}
				 }
			}
		}
		
		private function clicked(event:MouseEvent):void {
			//trace(event.currentTarget.char);
			//this.targetText.appendText(event.currentTarget.char);
			var currentposition:Number = this.targetText.caretIndex;
			var currenttext:String = this.targetText.text;
			var frontpart:String = this.targetText.text.substr(0,currentposition);
			var afterpart:String = this.targetText.text.substr(currentposition);
			switch (event.currentTarget.char) {
				case "Space Bar" :
				    frontpart = frontpart+" ";
					this.targetText.replaceText(0,this.targetText.length,frontpart+afterpart);
					this.targetText.setSelection(this.targetText.caretIndex+1,this.targetText.caretIndex+1);
					break;
				case "Tab" :
				     frontpart = frontpart+"\t";
					 this.targetText.replaceText(0,this.targetText.length,frontpart+afterpart);
					 this.targetText.setSelection(this.targetText.caretIndex+1,this.targetText.caretIndex+1);
					break;
				case "Backspace" :
				     frontpart = frontpart.substring(0,frontpart.length-1);
					 this.targetText.replaceText(0,this.targetText.length,frontpart+afterpart);
					 if(this.targetText.caretIndex == this.targetText.length)
					 {
					 this.targetText.setSelection(this.targetText.caretIndex,this.targetText.caretIndex);
					 }
					 else
					 {
						 this.targetText.setSelection(this.targetText.caretIndex-1,this.targetText.caretIndex-1);
						 
						 }
					break;
				case "Enter" :
					this.targetText+"\n";
					break;
				case "Shift" :
					shiftToggle();
					break;
				case "Caps Lock" :
					capsToggle();
					shift = false;
					break;
				default :
					addText(event.currentTarget.char, event.currentTarget.shiftChar,frontpart,afterpart);
					
			}
		}
		
		
		private function checkWidth(code:String):Number {
			var mediumKeys:Array = new Array('Tab','Enter','Backspace','Caps Lock');
			var shiftKey:String = "Shift";
			var spaceKey:String = "Space Bar";
			for (var m = 0; m < mediumKeys.length; m++) {
				if (code == mediumKeys[m]) {
					return 135;
				}
			}
			if (code == shiftKey) {
				return 170;
			}
			if (code == spaceKey) {
				return keyHolder.width;
			}
			return 65;
		}
		private function getNumberOfKeys():Number {
			var keyCounter:Number = 0;
			for (var i = 0; i < keyData.row.length(); i++) {
				keyCounter += keyData.row[i].key.length();
			}
			return keyCounter;
		}
	}
}