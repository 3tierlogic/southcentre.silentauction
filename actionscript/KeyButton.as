﻿package actionscript{

	import flash.display.MovieClip;
	import flash.display.*;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.events.*;
	import flash.geom.*;

	public class KeyButton extends MovieClip {

		public var keyWidth:uint = new uint;
		public var keyHeight:uint = new uint;
		public var code:uint = new uint;
		public var char:String = new String;
		public var shiftChar:String= new String;
		public var keyColor:Array= new Array;
		public var keyColor_hover:Array= new Array;
		public var fillType:String = GradientType.LINEAR;
		public var alphas:Array = [1, 1];
     	public var ratios:Array = [0x00, 0xFF];
     	public var matr:Matrix = new Matrix();
		public var cornerRadius:uint = 10;
		public var charColor:uint= new uint;
		public var lineColor:uint= new uint;
		public var charTextField:TextField = new TextField;
		public var shiftCharTextField:TextField = new TextField;
		private var charFormat:TextFormat = new TextFormat();
		private var backGround:MovieClip = new MovieClip;


		public function KeyButton() {
			// Setup the default for the variables.
			keyWidth = 90;
			keyHeight = 65;
			code = 0;
			char = '';
			shiftChar = '';
			keyColor = [0x2E478D, 0x84BDE7];
			//keyColor = [0xb91219, 0xde898c];
			keyColor_hover = [0x546db3, 0x99ccff];
			//keyColor_hover = [0xc99396, 0xe6babc];
			charColor = 0xFFFFFF;
			lineColor = 0xFFFFFF;
			matr.createGradientBox(20, 20, -1.57, 0, 0);
			//addEventListener(MouseEvent.CLICK, clicked);
			addEventListener(MouseEvent.MOUSE_OVER, hover);
			addEventListener(MouseEvent.MOUSE_OUT, leave);
			addEventListener(Event.ENTER_FRAME, checkStatus);
						addChild(backGround);

		}
		public function build():void {
			defaultState();
			addTextFields();
			//CustomKeyboard.textScreen.text = "";
		}
		private function reDraw(mouseStatus:String = ''):void {
			if (char == "Shift") {
				if (CustomKeyboard.shift) {
					hoverState();
				} else {
					defaultState();
				}
			} else if (char == "Caps Lock") {
				if (CustomKeyboard.caps) {
					hoverState();
				} else {
					defaultState();
				}
			} else {
				if (mouseStatus == "over") {
					hoverState();
				} else if (mouseStatus == "out") {
					defaultState();
				}
			}
		}
		private function hoverState() {
			backGround.graphics.clear();
			backGround.graphics.beginGradientFill(fillType,keyColor_hover,alphas,ratios, matr);
			backGround.graphics.lineStyle(1, lineColor-0x222222, 1, true);
			backGround.graphics.drawRoundRect(0, 0, keyWidth, keyHeight, cornerRadius);
			backGround.graphics.endFill();
		}
		private function defaultState() {
			backGround.graphics.clear();
			backGround.graphics.beginGradientFill(fillType,keyColor,alphas,ratios,matr);
			backGround.graphics.lineStyle(1, lineColor, 1, true);
			backGround.graphics.drawRoundRect(0, 0, keyWidth, keyHeight, cornerRadius, cornerRadius);
			backGround.graphics.endFill();
		}
		private function addTextFields():void {
			charFormat.font = "Verdana";
			charFormat.color = charColor;
			charFormat.size = 18;

			charTextField.width = keyWidth;
			charTextField.y = 20;
			if(char != "a" && char != "b" && char != "c" && char != "d" && char != "e" && char != "f" && char != "g" &&char != "h" && char != "i" && char != "j" && char != "k" && char != "l" && char != "m" && char != "n" && char != "o" && char != "p" &&char != "q" &&char != "r" &&char != "s" &&char != "t" &&char != "u" &&char != "v" &&char != "w" &&char != "x" &&char != "y" &&char != "z" ){
			charTextField.htmlText = char;
			}
			else
			{
			charTextField.htmlText = shiftChar;
			}
			charTextField.setTextFormat(charFormat);
			charTextField.autoSize = TextFieldAutoSize.CENTER;
			charTextField.selectable = false;
			charTextField.mouseEnabled = false;

			if(char != "a" && char != "b" && char != "c" && char != "d" && char != "e" && char != "f" && char != "g" &&char != "h" && char != "i" && char != "j" && char != "k" && char != "l" && char != "m" && char != "n" && char != "o" && char != "p" &&char != "q" &&char != "r" &&char != "s" &&char != "t" &&char != "u" &&char != "v" &&char != "w" &&char != "x" &&char != "y" &&char != "z" ){
			charFormat.size = 14;

			shiftCharTextField.width = keyWidth/2;
			shiftCharTextField.text = shiftChar;
			shiftCharTextField.setTextFormat(charFormat);
			shiftCharTextField.autoSize = TextFieldAutoSize.CENTER;
			shiftCharTextField.selectable = false;
			shiftCharTextField.mouseEnabled = false;
			}

			addChild(charTextField);
			addChild(shiftCharTextField);
		}
		private function checkStatus(event:Event) {
			reDraw();
		}
		private function hover(event:MouseEvent) {
			reDraw("over");
		}
		private function leave(event:MouseEvent) {
			reDraw("out");
		}
	}
}